---
Title: Contact
---

# Contact

The best way to stay in touch is by joining our [Mailing List](https://groups.google.com/forum/#!forum/livecodenyc). It is the communication channel we use to send announcements about shows and upcoming meetings.

When IRL meetings are not possible, we host e-meetings on [Discord](https://discord.gg/fcrAKbg).

For social media, we primarily use [Twitter](https://twitter.com/livecodenyc)
and [Instagram](https://instagram.com/livecodenyc).

# Global Community

- Forums
  - [TOPLAP Forum](https://forum.toplap.org)
  - [llllllll](https://llllllll.co/)
- Chat
  - [TOPLAP Chat](https://chat.toplap.org)

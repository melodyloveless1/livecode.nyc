---
name: Leo Foletto
image: https://github.com/leofltt.png
links:
    website: https://leonardofoletto.com
    twitter: https://twitter.com/leofltt
    instagram: https://www.instagram.com/leofltt/
---

Sound designer, audio programmer, multimedia artist. Making music with Csound and Haskell.
